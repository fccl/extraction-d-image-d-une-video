# À quoi ce script sert ?

Le script a pour utilité de découper une vidéo en images avec un délai entre images défini. Utile par exemple si vous utilisez une caméra qui n'a pas d'option pour créer un intervalle entre prises de photos.


# Comment utiliser le script ?

Se rendre dans le dossier où se situe le script, placer la video dans le meme dossier. Les images seront extraites dans un dossier Export_ suivi du nom de la vidéo. 

# Prérequis : 
Ouvrir un terminal dans le dossier. 
## Rendre ce fichier exécutable, rentrer la commande :
`chmod u+x FR_ExtractionVideoImages.sh`
## Avoir le paquet exiftool intallé, si ce n'est pas le cas rentrer : 
`sudo apt-get intall libimage-exiftool-perl`
## Lancer le script
`./FR_ExtractionVideoImages.sh nomVidéoÀExtraire DelaiVouluEntreImageEnSeconde x`


> Exemple : ./FR_ExtractionVideoImages.sh video.mov 3 x

Les images de la vidéo video.mov seront extraites toutes les 3 secondes.



Des options supplémentaires sont disponibles en retirant le x final. 
- Choix du dossier où sera créé le dossier Export_ et où seront extraites les images
- Choix du temps de début et de fin d'extraction.

