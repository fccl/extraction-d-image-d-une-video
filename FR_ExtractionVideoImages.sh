#! /bin/bash


#Placer le script dans le dossier où se trouve la vidéo et les images seront extraites dans un dossier Export_nomDeLaVideo
#Rentrer dans le terminal : ./ExtractionVideoImages nomVidéoAExtraire DelaiVouluEntreImageEnSeconde x


#Il faut le paquet Exiftool pour que ce script fonctionne


#Des paramètrages supplémentaires sont disponibles en enlevant le x en fin de commande.



CorrectionNombreDecimal()
{
	#Supprimer le 0 comme premier caractère d'une chaine de caractere 
	changement=0
	case $texte in
		"00")
			texte2=0
			changement=1
			;;
		"01")
			texte2=1
			changement=1
			;;

		"02")
			texte2=2
			changement=1
			;;

		"03")
			texte2=3
			changement=1
			;;

		"04")
			texte2=4	
			changement=1
			;;

		"05")
			texte2=5
			changement=1
			;;

		"06")
			texte2=6
			changement=1
			;;

		"07")
			texte2=7
			changement=1
			;;

		"08")
			texte2=8
			changement=1
			;;

		"09")
			echo
			texte2="9"
			changement=1
			;;

	esac
}


#Vérification des variables nécéssaires

	#Vérification de la vidéo comme premier argument
	if [ $# -ge 1 ];then
		Video=$1
	else
		read -p 'Nnom de la vidéo : ' Video 
	fi


	#Vérification du délai entre chaque images comme second argument
	if [ $# -ge 2 ];then
		SecondesEntreImages=$2
	else
		read -p 'Délai entre chaque images (secondes) : ' SecondesEntreImages 
	fi
	
	OLDIFS=$IFS
	IFS=	
	#Vérification de la prise de vue de la vidéo
	PriseDeVueVideo=$(exiftool -CreateDate -s3 $Video)
	if [ -n $PriseDeVueVideo ];then
		 
		IFS=" :"
		read -r Annee Mois Jour Heure Minute Seconde <<< $PriseDeVueVideo
	else
		IFS=" :"
		read -p 'Date de prise de la vidéo (AAAA:MM:JJ hh:mm:ss) : ' Annee Mois Jour Heure Minute Seconde 
	fi
	IFS=$OLDIFS


	#Vérification de la durée de la vidéo 
	Duree=$(exiftool -MediaDuration -s3 $Video)
	if [ -n $Duree ];then
		IFS=" :" 
		read -r  DureeHeure DureeMinute DureeSeconde <<< $Duree
		
	else
		IFS=" :"
		read -p 'Durée de la vidéo (hh:mm:ss) : ' DureeHeure DureeMinute DureeSeconde


	fi

	IFS=$OLDIFS

		#Utilisation de IFS (Internal Field Separator) : 
		#IFS est une variable du shell permetant de délimiter les champs reconnu par l'interpréteur (ici read). Par défaut elle comprend : l'espace, la tabulation(\t) et le saut de ligne (\n). 
		#1- On sauvegarde la valeur par défaut dans une variable OLDIFS : OLDIFS=$IFS
		#2- La variable PriseDeVueVideo est composée d'espace et qu'on ne souhaite pas qu'ils soit pris en compte, on supprime tous les séparateur de IFS : IFS=
		#3- Il est nécéssaire de répartir les différentes données de la variable PriseDeVueVidéo dans plusieurs variables (Année, Mois...) séparée par des espaces ou ":". On fixe alors l'IFS avec ces deux délimiteurs. : IFS=" :". 
		#4- Pour finir, et faire en sorte que le changement de l'IFS ne reste que dans le script, redonner à l'IFS sa valeur par défaut : IFS=$OLDIFS.


	
	#Vérification du bon format des dates/heures
	
	texte=$DureeHeure
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		DureeHeure=$texte2
	fi
	texte=$DureeMinute
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		DureeMinute=$texte2
	fi
	texte=$DureeSeconde
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		DureeSeconde=$texte2
	fi
	
	texte=$Annee
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		Annee=$texte2
	fi
	texte=$Mois
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		Mois=$texte2
	fi
	texte=$Jour
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		Jour=$texte2
	fi
	texte=$Heure
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		Heure=$texte2
	fi
	texte=$Minute
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		Minute=$texte2
	fi
	texte=$Seconde
	CorrectionNombreDecimal
	if [ $changement = "1" ];then
		Seconde=$texte2
	fi




	echo "Récapitulatif :"
	echo "	Vidéo à extraire : $Video"
	echo "	Temps (secondes) entre chaques images : $SecondesEntreImages"
	echo "	Duree de la vidéo : $DureeHeure h $DureeMinute m $DureeSeconde s"
	echo "	Date de prise de vue :  $Heure h $Minute m $Seconde   le $Jour / $Mois / $Annee";echo




#Variables supplémentaires

	let "ConversionDuree=DureeHeure*3599+DureeMinute*60+DureeSeconde"
	let "t=0"

#Proposition des variables optionnelles : 

	choix="x"
	chemin="./"
	let "TempsMin = 0"
	let "TempsMax = ConversionDuree"

	if [ $# -ge 3 ] && [ $3 = $choix ];then
		echo "Script par défaut"
	else
		echo
		echo "Paramètres supplémentaires :"
		echo "   a- Extraction des images dans un dossier précis"
		echo "   b- Extraction de la vidéo à partir d'un temps donné"
		echo "   c- Extraction de la vidéo jusqu'à un temps donné"
		echo "   x- Paramètres par défaut"
		echo
		read -p " choix : " choix
		while [ $choix != "x" ];do
			if [ $choix = "a" ];then
				read -p "Entrer le chemin voulu pour le dessier " chemin
			elif [ $choix = "b" ];then
				read -p "Temps de début d'extraction (hh mm ss) : " minHeure minMinute minSeconde
				let "TempsMin = minHeure*3600+minMinute*60+minSeconde"
				if [ $t -lt $ConversionDuree ];then
					let "t+=TempsMin"
					let "Seconde+=TempsMin"
				else
					let "TempsMin=0"
					echo "Temps de début d'extraction supérieur à la durée de vidéo. Temps non pris en compte"
				fi
			elif [ $choix = "c" ];then
				read -p "Temps d'arret d extraction (hh mm ss) :"  maxHeure maxMinute maxSeconde
				let "TempsMax=maxHeure*3600+maxMinute*60+maxSeconde"
				if [ $TempsMax -lt $ConversionDuree ];then
					let "ConversionDuree = TempsMax"
				else
					echo "Temps de fin d'extraction supérieur à la durée de vidéo. Temps non pris en compte"
				fi
			fi
			echo
			read -p " Autre choix ? (x si non) : " choix
		done
	fi		



VerificationAnneeBissextile()
{
	let "year = Annee"
	let "hyp1 = year%4"
	let "hyp2 = year%100"
	let "hyp3 = year%400"

	if  [ $hyp1 -eq 0 ] && [ $hyp2 -ne 0 ] || [ $hyp3 -eq 0 ];then
		let "Bissextile=1"
	else
		let "Bissextile=0"
	fi
}



CorrectionFormatDate()
{
	#Cette fonction intervient juste après le rajout de une à plusieurs secondes à la variable "Seconde".
	#Il faut donc vérifier que les secondes  ne dépasses pas la minute et sinon, les remettre à zéro et augmenter les minutes.
	#Et ainsi de suite jusqu'à l'année en tenant compte des aléas de jours suivant le mois et l'année. 
	while [ $Seconde -ge 60 ];do
		let "Seconde-=60"
		let "Minute+=1"
		while [ $Minute -ge 60 ];do	
			let "Minute-=60"	
			let "Heure+=1"
			while [ $Heure -ge 24 ];do
				let "Heure-=24"
				let "Jour+=1"
				case $Mois in
					1 | 3 | 5 | 7 | 8 | 10 | 12)
						if [ $Jour -gt 31 ];then
							let "Jour-=30"
							let "Mois+=1"		
							if [ $Mois -gt 12 ];then
								let "Mois-=11"
								let "Annee+=1"
							fi
						fi;;
					4 | 6 | 9 | 11)
						if [ $Jour -gt 30 ];then
							let "Jour-=29"
							let "Mois+=1"		
						fi;;
					2)
						VerificationAnneeBissextile
						if [ $Jour -gt 29 ] && [ $Bissextile -eq 1 ];then
							let "Jour-=28"
							let "Mois+=1"
						elif [ $Jour -gt 28 ] && [ $Bissextile -eq 0 ];then
							let "Jour-=27"
							let "Mois+=1"
						fi;;
					*)	
						;;
				esac	
			done	
		done 
	done

	#Cette fonction intervient juste après la correction de l'horaire de prise de vue de la vidéo.
	#On retire la durée de la vidéo à l'heure d'enregistrement de la vidéo pour avoir l'heure de début de vidéo.
	#Il faut donc vérifier que les secondes  ne soient pas négatives et sinon, les remettre à zéro et diminuer les minutes.
	#Et ainsi de suite jusqu'à l'année en tenant compte des aléas de jours suivant le mois et l'année. 
	while [ $Seconde -lt 0 ];do
		let "Seconde+=60"
		let "Minute-=1"
		while [ $Minute -lt 0 ];do	
			let "Minute+=60"	
			let "Heure-=1"
			while [ $Heure -lt 0 ];do
				let "Heure+=24"
				let "Jour-=1"
				case $Mois in
					5 | 7 | 10 | 12)
						if [ $Jour -lt 1 ];then
							let "Jour+=30"
							let "Mois-=1"		
							if [ $Mois -lt 1 ];then
								let "Mois+=12"
								let "Annee-=1"
							fi
						fi;;
					1 | 2 | 4 | 6 | 8 | 9 | 11)
						if [ $Jour -lt 1 ];then
							let "Jour+=31"
							let "Mois-=1"	
							if [ $Mois -lt 1 ];then
								let "Mois+=12"
								let "Annee-=1"
							fi
	
						fi;;
					3)
						VerificationAnneeBissextile
						if [ $Jour -lt 1 ] && [ $Bissextile -eq 1 ];then
							let "Jour+=29"
							let "Mois-=1"
						elif [ $Jour -lt 1 ] && [ $Bissextile -eq 0 ];then
							let "Jour+=28"
							let "Mois-=1"
						fi
						if [ $Mois -lt 1 ];then
							let "Mois+=12"
							let "Annee-=1"
						fi;;


					*)	
						;;
				esac	
			done	
		done 
	done

}



#Boucle principale :

	CorrectionFormatDate
	mkdir -p $chemin/Export_$Video/
	while [ $t -lt $ConversionDuree ];do
		echo "$Annee:$Mois:$Jour $Heure:$Minute:$Seconde"
		
		#Extraire une image au temps t - Commentaire de la commande seulement si risque de plantage. 
		ffmpeg -ss $t -i $Video -r 1 -loglevel panic -frames:v 1 Image.jpg 
		
		#Changer les dates des créations
		exiftool -AllDates="$Annee:$Mois:$Jour $Heure:$Minute:$Seconde" Image.jpg
	
		#Renommer et copier dans le dossier Export
		mv Image.jpg $chemin/Export_$Video/
		exiftool "-FileName<DateTimeOriginal" -d "%Y%m%d_%H%M%S.jpg" $chemin/Export_$Video/Image.jpg
		
		#Préparation du prochain temps de travail
		let "t += SecondesEntreImages"
		let "Seconde += SecondesEntreImages"
		CorrectionFormatDate
	done

	#Une Image.jpg_original se crée au temps t=0. Elle n'est pas donc utile. 
	rm -v "Image.jpg_original"
